package com.example.recuexamenjava;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private EditText txtNombreCliente;
    private Button btnCotizacion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        iniciarComponentes();
    }

    //Enviando parametros de nombre de cliente
    public void Enviar(View v){
        String txtNombreClienteValue = txtNombreCliente.getText().toString();

        if (txtNombreClienteValue.isEmpty()) {
            // Code block executed when any field is empty or the bank is not valid
            Toast.makeText(getApplicationContext(), "Falta información en algunos campos o el banco no es válido", Toast.LENGTH_SHORT).show();
        } else {
            // Code block executed when all fields are filled and the bank is valid
            Bundle bundle = new Bundle();
            bundle.putString("nombre", txtNombreClienteValue);

            Intent intent = new Intent(this, CotizacionActivity.class);
            intent.putExtras(bundle);

            startActivity(intent);
        }
    }

    //Iniciando componentes
    public void iniciarComponentes(){
        txtNombreCliente = findViewById(R.id.txtNombreCliente);
        btnCotizacion = findViewById(R.id.btnCotizacion);

    }
}