package com.example.recuexamenjava;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

public class CotizacionActivity extends AppCompatActivity {

    private TextView lblCliente;
    private TextView lblFolio;

    private EditText txtDescripcion;
    private EditText txtValorAuto;
    private EditText txtPagoInicial;

    private RadioButton rbPlazo12;
    private RadioButton rbPlazo18;
    private RadioButton rbPlazo24;
    private RadioButton rbPlazo36;

    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnRegresar;

    private TextView lblPagoMensual;
    private TextView lblEnganche;

    //Para inicializar los datos de la clase CuentaBanco
    private Cotizacion cotizacion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cotizacion);
        iniciarComponentes();

        //Recibir los datos del MainActivity
        //Mapeo de valores que ingresas en el getIntent
        //Extension de datos que viene en el intent
        Bundle datos = getIntent().getExtras();
        String nombre = datos.getString("nombre");

        cotizacion = new Cotizacion(nombre);

        // Actualizar para remplazar los componentes de la interfaz de usuario con los datos recibidos
        lblCliente.setText(nombre);
        btnCalcular.setOnClickListener(){

        };

    }



    private void iniciarComponentes() {
        lblCliente = findViewById(R.id.lblCliente);
        lblFolio = findViewById(R.id.lblFolio);
        txtDescripcion = findViewById(R.id.txtDescripcion);
        txtValorAuto = findViewById(R.id.txtValorAuto);
        txtPagoInicial = findViewById(R.id.txtPagoInicial);
        rbPlazo12 = findViewById(R.id.rbPlazo12);
        rbPlazo18 = findViewById(R.id.rbPlazo18);
        rbPlazo24 = findViewById(R.id.rbPlazo24);
        rbPlazo36 = findViewById(R.id.rbPlazo36);
        rbPlazo18 = findViewById(R.id.rbPlazo18);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);
        lblPagoMensual = findViewById(R.id.lblPagoMensual);
        lblEnganche = findViewById(R.id.lblEnganche);
    }
    private boolean validarCampos() {
        String descripcion = txtDescripcion.getText().toString();
        String descripcion = txtDescripcion.getText().toString();


        return !descripcion.isEmpty();

    }


}